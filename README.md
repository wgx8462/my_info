# 이력서
## 소개
<img src="images/xIaIHY0lTTSDwqano8dkUA.png" width= 150 alt="내사진"/>

* **이름 :** 김기범
* **군필여부 :** 3포병여단 759다련장대대 FDC 만기전역
* **취미 :** 게임
* **Email :** wgx8462@gmail.com
* **Gitlab :** [gitlab.com/wgx8462](https://gitlab.com/wgx8462)

```javascript
가장 위대한 예술가도 한 때는 초심자 였다.
```

## 회사경력
* **신광기계 제작소**(2019 ~ 2023)

## 학력
* 안산대학교 시각미디어디자인과(2012 입학)
* 안산공업고등학교(2009 ~ 2012)

## 사용기술
![](images/skill.png)

### Backend Development
클라이언트에 필요한 기술을 테스트하고 검증할 수 있을 정도의 서버 구현 스킬을 가지고 있습니다.

* Java
* Spring Boot

### Frontend Development
간단한 프로토타입을 만들 수 있을 정도의 스킬을 가지고 있습니다.

* Nuxt.js
* Vue.js
* React.js
* Next.js
* Dart
* Flutter

### DevOps / Infrastructure
서비스의 배포, 운영, 및 인프라 관리를 위한 기술을 보유하고 있습니다.

* GCP
* Kubernetes (k8s)
* Jenkins
* Docker
* PostgreSQL

### 그 외

* 커뮤니케이션 : Slack
* 버전관리 : Gitlab
* 로그관리 : ELK

## 프로젝트 이력
<img src="images/ic_launcher_adaptive_fore.png" width="80" alt="VERSUS"/><br>
### VERSUS
* **소개 :** 둘중 하나 선택이 어려울 때 다른사람과 함께 해결해보세요
* **기간 :** 2023.12 ~ 2024.02
* **링크 :** [자세히](https://curse-tern-8be.notion.site/versus-2eed9feffbe7462ca572fb22030da50d?pvs=4)
* **영상 :** [동영상](https://youtu.be/5ms8nA3Whxw?si=xT0Px2G--yxkXOsI)
---
<img src="images/logo2.png" width="80" alt="내 알바야"/><br>
### 내 알바야
* **소개 :** 사장님과 알바를 연결해주는 내 알바야
* **기간 :** 2024.03 ~ 2024.05
* **링크 :** [자세히](https://curse-tern-8be.notion.site/05ab2afe02dc4199acafad48d58a0787?pvs=4)
* **영상 :** [사장님 앱&웹](https://youtu.be/TT2fAmvc3mc?si=U9S7DBFKiEwGICTB)
* **영상 :** [알바생 & 관리자](https://youtu.be/-GLuWnb4N2M?si=p73Zp63UpUEjwubT)

---
<img src="images/jifeejigi.png" width="80" alt= "지피지기"/><br>
### 자동차 견적 / 역산 지피지기
* **소개 :** 고객에게 최적의 선택을 제공할 수 있는 전략을 기획하는 데 초점을 맞추고 있습니다.
* **기간 :** 2024.08 ~ 2024.09
* **링크 :** [자세히](https://curse-tern-8be.notion.site/cfbbb74c171c43f380f619fd8d46a6ab?pvs=4)
* **PDF :** [프로젝트 CRM](https://drive.google.com/file/d/1T2zSns7KcnQclh6SXm6DqNTM72W7BOG7/view?usp=sharing)
* **PDF :** [프로젝트 견적 역산 집중](https://drive.google.com/file/d/1ojrPaZlrHJBodR2-z9xcoXrrBLYbi20_/view?usp=sharing)
